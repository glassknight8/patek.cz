function restoreDarkmode() {
	const prefersDarkMode = window.matchMedia("(prefers-color-scheme: dark)");
	const elem = document.getElementById("content");

	if (!document.cookie.includes("dark-mode=")) {
		if (elem && prefersDarkMode.matches) {
			elem.classList.add("dark-mode");
		}
	}

	if (elem && document.cookie.includes("dark-mode=on")) {
		elem.classList.add("dark-mode");
	}
}

function toggleDarkmode() {
	const elem = document.getElementById("content");
	if (elem) {
		const state = elem.classList.toggle("dark-mode");
		if (state) {
			// 31536000 = 1 year; not setting max-age would make it session bound
			document.cookie = "dark-mode=on; path=/; max-age=31536000; secure; samesite=strict";
		} else {
			// We have to set the off preference as well, so it can override the OS setting
			document.cookie = "dark-mode=off; path=/; max-age=31536000; secure; samesite=strict";
		}
	}
}

document.addEventListener("DOMContentLoaded", restoreDarkmode);
document.addEventListener("DOMContentLoaded", () => {
	for (const toggler of document.getElementsByClassName("dark-mode-toggler")) {
		toggler.addEventListener("click", toggleDarkmode);
	}
});
