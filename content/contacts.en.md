---
title: "Contact us"
menu:
  main:
    weight: 20
---
The best way to get in touch with us is our Telegram group:
[t.me/patekvpatek](https://t.me/patekvpatek).

We also have an email group: [patek@gbl.cz](mailto:patek@gbl.cz)

[Ing. Eva Kospachová](mailto:e.kospachova@gbl.cz) is in charge of Pátek with
[Vojtěch Káně](mailto:vojtech.kane@gbl.cz), [Jan
Černohorský](mailto:jan.cernohorsky@gbl.cz), [Tomáš
Kysela](mailto:tomas.kysela@gbl.cz) and [Šimon
Šustek](mailto:simon.sustek@gbl.cz).
