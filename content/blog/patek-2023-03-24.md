---
title: "Pátek 24. 3. 2023"
date: 2023-03-24T20:06:09+26:00
draft: false
authors: [ "Glassknight" ]
tags: []
---
Další Pátek blog - cože, po tak dlouhé době!? Je tomu tak! Tato sporadičnost není způsobená tím, že by se Pátek nekonal, ale tím, že se musí mezi pisateli vždy předat žezlo, a to nějakou dobu trvá. A tentokrát padlo na mne.

Ale teď už k tomu, co se dnes stalo:

Pátek začal pomaleji než obvkle, řešili se totiž ve fyzlabu důležité věci, a takže jsme museli kolem chodit po špičkách. Jakmile se vše dořešilo a delegace odešla, rozlehl se po škole bujarý smích a hlukot tak synonymní s Pátkem, jako budova gymnázia.

Diskutovalo se o všem možném, co já jsem pochytil tak byla diskuze o formátování texu a pdf, a základy lingebry. Dozvěděl jsem se, že v pdf existuje fíčura: čára o síle 0, která se ukáže jako nejtenčí možná. To je buď 1 px na jakýmkoliv displeji, nebo třeba čára o síle jedné kapičky toneru když se to dá tisknout.

Já osobně jsem si hrál už třetí týden v řadě s vyšívacím strojem, který se mi čím dál tím míň seká! Už jsem měl skoro hotovou svoji první hezkou výšivku, ale v cílové rovince se rozhodl se seknout, a tak jsem ho musel zastavit, aby mi nezníčil jehlu jako na minulém pátku.

Ještě proběhl částečný úklid videosálku, ale čeká se hlavně na maturanty až si  odnesou svoje věci po plesu.