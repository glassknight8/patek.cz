---
title: "Co se dělo na prvním Pátku tohoto školního roku"
date: 2019-09-08T14:22:42+02:00
draft: false
authors: [ "Greenscreener" ]
tags: [ "schuze" ]
---
První Pátek tohoto školního roku byl skutečně nabitý a plný nefalšovaného Pátečního ducha. Na začátku jsme byli šokováni počtem nových členů, kteří nás přišli navštívit. Po krátkém projevu, který proběhl v úterý o velké přestávce v učebně primy, se nám na Pátku objevilo **deset** nových tváří, nadšených pro vše, co jsme jim ukázali. Vojta měl na začátku přednášku o 3D tiskárně, která sklidila nemalý úspěch a vyvolala neutuchající nadšení. Dalším šokem, který nás čekal, byla kopírka, kterou našel jeden z členů zahozenou po cestě ze školy. Je sice funkční, ale velmi často se v ní zasekává papír, proto je otázkou, jestli pro ni najdeme využití, nebo jestli bude rozebrána a přetvořena na něco jiného. Poté, co se trochu uklidnila atmosféra, proběhly přednášky o [Kybersoutěži a PátekCTF]({{< relref "/talks/patekctf1" >}}) a o [projektové dokumentaci]({{< relref "/talks/projectdocs" >}}). Nakonec jsme rozebrali pár starých elektrospotřebičů a poslední členové se rozešli domů.

Děkuji všem za účast a těším se na další takové Pátky,

GS
