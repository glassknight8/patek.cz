---
title: "Vojta"
fullname: "Vojta Káně"
---
I am one of the founding members of Pátek, currently retired event though I try to be in contact with the community as much as possible.

More can be found in the [Czech version]({{< relref path="." lang="cs" >}}) or on [my website](https://vkane.cz).
